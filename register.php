<?php
    // Start the session
    session_start();

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["firstname"])) {
            $_SESSION["firstnameErr"] = "First name is required";
          }
        else {
            $_SESSION["firstname"] = test_input($_POST["firstname"]);
            // check if name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$_SESSION["firstname"])) {
                //$_SESSION["firstname"] = "";
                $_SESSION["firstnameErr"] = "Only letters and white space allowed";
            }
            else {
                $_SESSION["firstnameErr"] = "";
            }
          }

          if (empty($_POST["lastname"])) {
                $_SESSION["lastnameErr"] = "Last name is required";
              }
            else {
                $_SESSION["lastname"] = test_input($_POST["lastname"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/",$_SESSION["lastname"])) {
    //                $_SESSION["lastname"] = "";
                    $_SESSION["lastnameErr"] = "Only letters and white space allowed";
                }
                else {
                    $_SESSION["lastnameErr"] = "";
                }
              }

              if (empty($_POST["mail_adress"])) {
                  $_SESSION["mail_adressErr"] = "Email is required";
              }
              else {
                  $_SESSION["mail_adress"] = test_input($_POST["mail_adress"]);
                  // check if e-mail address is well-formed
                  if (!filter_var($_SESSION["mail_adress"], FILTER_VALIDATE_EMAIL)) {
                      $_SESSION["mail_adressErr"] = "Invalid email format";
                  }
                  else {
                      $_SESSION["mail_adressErr"] = "";
                  }
              }
            }

      function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

    if (substr($_SESSION["firstnameErr"], 0, 1) == 'O' || $_POST["firstname"] == "") {
        header('Location: ../index.php');
        exit ();
    }
    else {
        $_SESSION["firstnameErr"] = "";
    }

    if (substr($_SESSION["lastnameErr"], 0, 1) == 'O' || $_POST["lastname"] == "") {
        header('Location: index.php');
        exit ();
    }
    else {
        $_SESSION["lastnameErr"] = "";
    }

    if (substr($_SESSION["mail_adressErr"], 0, 1) == 'I' || $_SESSION["mail_adress"] == "") {
        header('Location: index.php');
        exit ();
    }
    else {
        $_SESSION["mail_adressErr"] = "";
    }

    if (empty($_POST["date"])) {
    $dateErr = "Date of birth is required";
  } else {
    $date = test_input($_POST["date"]);
  }

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="PHP validation registration">
    <meta name="keywords" content="php, validation, registration">
    <meta name="author" content="Nemanja Stojanović">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>PHP validation registration</title>
  </head>
  <body>

  <div class="form">

    <h1 class="form--head">Here u go:</h1>

    <div class="form__container">
      <p>Your first name is: <?php echo $_SESSION["firstname"]; ?></p>
      <p>Your last name is: <?php echo $_SESSION["lastname"]; ?></p>
      <p>You where born on: <?php echo $_SESSION["date"]; ?></p>
      <p>Your email is: <?php echo $_SESSION["mail_adress"]; ?></p>
      <p>Return fo form <?php echo "<a href=\"index.php\">click here</a>";?></p>
    </div>

  </div>


  </body>
</html>
