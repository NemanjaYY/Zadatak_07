<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>PHP Form homework</title>
</head>
<body>

<?php

    $name = $date = $mail = $city = "";
    $nameErr = $dateErr = $mailErr = $cityErr = "";

    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['date'])) { $date = $_GET['date']; }
    if (isset($_GET['mail'])) { $mail = $_GET['mail']; }
    if (isset($_GET['city'])) { $subject = $_GET['city']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['dateErr'])) { $dateErr = $_GET['dateErr']; }
    if (isset($_GET['mailErr'])) { $mailErr = $_GET['mailErr']; }
    if (isset($_GET['cityErr'])) { $cityErr = $_GET['cityErr']; }

?>

    <main>
      <div class="form">
        <h1>CONTACT US</h1>
        <div class="form__container">
        <form action="register2.php" method="post">
            <label for="fname">Your Name/Lastname<span>*<?php echo $nameErr;?></span></label>
            <input type="text" name="name" value="<?php echo $name; ?>" placeholder="Full name"><br>

            <label for="lname">Date of Birth: <span>*<?php echo $dateErr;?></span></label><br>
            <input type="date" name="date" value="<?php echo $date; ?>" placeholder="Date of birth"><br><br>

            <label for="date">Your mail <span>*<?php echo $mailErr;?></span></label>
            <input type="text" name="mail" value="<?php echo $mail; ?>" placeholder="Your e-mail"><br>

            <label for="date">Where are you from? <span>*<?php echo $cityErr;?></span></label>
            <input type="text" name="subject" value="<?php echo $city; ?>" placeholder="Subject"><br>

            <label for="message">Few words...</label>
            <textarea name="message" rows="8" cols="25" placeholder="Feel free to type any additional message!"></textarea><br>
            <button type="submit" name="submit">SEND MAIL</button>
        </form>
        </div>
      </div>
    </main>
    <?php
    /*
    $fullUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if(strpos($fullUrl, "contactform=empty") == true) {
        echo "<p>You did not fill in all fields!</p>";
        exit();
    }

    elseif(strpos($fullUrl, "contactform=char") == true) {
        echo "<p>You used invalid characters!</p>";
        exit();
    }

    elseif(strpos($fullUrl, "contactform=invalidemail") == true) {
        echo "<p>You used an invalid e-mail!</p>";
        exit();
    }

    elseif(strpos($fullUrl, "mail=sent") == true) {
        echo "<p class='succes'>Your e-mail has been sent!</p>";
        exit();
    } */

    ?>
</body>
</html>
