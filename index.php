<?php
session_start();

  if (count($_SESSION) == 0) {

      $_SESSION["firstname"] = "";
      $_SESSION["lastname"] = "";
      $_SESSION["mail_adress"] = "";
      $_SESSION["firstnameErr"] = "";
      $_SESSION["lastnameErr"] = "";
      $_SESSION["mail_adressErr"] = "";
      $_SESSION["date"]="";
      $_SESSION["dateErr"]="";
    }

    $firstname = $_SESSION["firstname"];
    $firstnameErr = $_SESSION["firstnameErr"];
    $lastname = $_SESSION["lastname"];
    $lastnameErr = $_SESSION["lastnameErr"];
    $mail_adress = $_SESSION["mail_adress"];
    $mail_adressErr = $_SESSION["mail_adressErr"];
    $dateErr = $_SESSION["dateErr"];

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="PHP validation registration">
    <meta name="keywords" content="php, validation, registration">
    <meta name="author" content="Nemanja Stojanović">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>PHP validation registration</title>

  </head>
  <body>

  <div class="form">

    <h1 class="form--head">Contact Form</h1>

    <div class="form__container">
      <form action="register.php" method="post">

        <label for="fname">First Name <span>*</span></label>
        <input type="text" id="firstname" name="firstname" placeholder="Your name.." value="<?php echo $firstname; ?>"> <?php echo $firstnameErr; ?>

        <label for="lname">Last Name <span>*</span></label>
        <input type="text" id="lastname" name="lastname" placeholder="Your last name.." value="<?php echo $lastname; ?>"> <?php echo $lastnameErr; ?>

        <label for="date">Date <span>*</span></label>
        <input type="date" name="date" placeholder="Date of birth" value="<?php echo $date; ?>"><?php echo $dateErr; ?> <br><br>

        <label for="lname">Mail Adress <span>*</span></label>
        <input type="text" id="mail_adress" name="mail_adress" placeholder="Petar_Petrovic@gmail.com" value="<?php echo $mail_adress; ?>"> <?php echo $mail_adressErr; ?>

        <label for="subject">What's on your mind?</label>
        <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

        <input type="submit" value="submit">
      </form>
    </div>

  </div>


  </body>
</html>
