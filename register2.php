<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>PHP Form homework</title>
</head>
<body>
    <div class="register-box">
<?php

function checkEmailAdress($mail) {
  if (strpos($mail, '@gmail.com'))
      return false; // ukoliko nije pronasao string 'gmail' u promenljivoj 'mail', izbacuje gresku (mailErr);
  else
      return true;
}

// define variables and set to empty values

$nameErr = $dateErr = $mailErr = $cityErr = "";
$name = $date = $mail = $city = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   if (empty($_POST["name"])) {
    $nameErr = "Name is required";
  } else {
    $name = test_input($_POST["name"]);

    // check if name only contains letters and whitespace

    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed";
    }
    elseif (strlen($name) < 2) {
      $nameErr = "Fullname must have at least 2 characters!";
    }
  }

  if (empty($_POST["date"])) {
    $dateErr = "Date of birth is required";
  } else {
    $date = test_input($_POST["date"]);
  }

  if (empty($_POST["subject"])) {
    $subjectErr = "Subject is required";
  } else {
    $subject = test_input($_POST["subject"]);

    // check if name only contains letters and whitespace

    if (!preg_match("/^[a-zA-Z ]*$/",$city)) {
      $cityErr = "City is required";
    }
  }

  if (empty($_POST["mail"])) {
    $mailErr = "Email is required";
  } else {
    $mail = test_input($_POST["mail"]);

    // check if e-mail address is well-formed

    if (!filter_var($mail, FILTER_VALIDATE_EMAIL) /* || strpos($mail, '@gmail.com') === false */) {
      $mailErr = "Invalid email format";
    }
    elseif (checkEmailAdress($mail)) {
      $mailErr = "Not a valid gmail address!";
    }
    elseif (strlen($mail) < 20) {
      $mailErr = "E-mail must have at least 20 characters!";
    }
  }

  if (!empty ($nameErr) || !empty ($dateErr) || !empty($mailErr) || !empty($subjectErr)) {
    $params = "&name=" . urlencode($_POST["name"]);
    $params = "&date=" . urlencode($_POST["date"]);
    $params .= "&mail=" . urlencode($_POST["mail"]);
    $params .= "&city=" . urlencode($_POST["city"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&dateErr=" . urlencode($dateErr);
    $params .= "&mailErr=" . urlencode($mailErr);
    $params .= "&cityErr=" . urlencode($cityErr);

    header("Location: index2.php?empty" . $params);
  }  else {
    echo "<h2>Your Input:</h2>";
    echo "<b>Name: </b>" . $_POST['name'];
    echo "<br>";

    echo "<b>Date of birth: </b>" . $_POST['date'];
    echo "<br>";
    echo "<b>You were born on: </b>" . date("l", strtotime($date)); // "l" A full textual representation of the day of the week
    echo "<br>";

    echo "<b>Email: </b>" . $_POST['mail'];
    echo "<br>";

    echo "<b>City: </b>" . $_POST['city'];
    echo "<br>";

    echo "<b>Additional message: </b>" . $_POST['message'];
    echo "<br>";
    echo "<br>";

    echo "<a href=\"index2.php\">Return to form</a>";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
  </div>
</body>
</html>
